#include "boxshader.h"

BoxShader::~BoxShader()
{
    textureArray_->destroy();
    delete textureArray_;
}

void BoxShader::initsize(int width, int height, QImage &left, QImage &bebind, QImage &right, QImage &front, QImage &top, QImage &bottom)
{
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vert");
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.frag");
    program_.link();

    textureArray_ = new QOpenGLTexture(QOpenGLTexture::Target2DArray);
    textureArray_->create();
    textureArray_->setSize(width,height);
    textureArray_->setLayers(6);
    textureArray_->setFormat(QOpenGLTexture::RGB8_UNorm);
    textureArray_->allocateStorage();

    textureArray_->setData(0,0,QOpenGLTexture::RGB,QOpenGLTexture::UInt8,left.convertToFormat(QImage::Format_RGB888).constBits());
    textureArray_->setData(0,1,QOpenGLTexture::RGB,QOpenGLTexture::UInt8,bebind.convertToFormat(QImage::Format_RGB888).constBits());
    textureArray_->setData(0,2,QOpenGLTexture::RGB,QOpenGLTexture::UInt8,right.convertToFormat(QImage::Format_RGB888).constBits());
    textureArray_->setData(0,3,QOpenGLTexture::RGB,QOpenGLTexture::UInt8,front.convertToFormat(QImage::Format_RGB888).constBits());
    textureArray_->setData(0,4,QOpenGLTexture::RGB,QOpenGLTexture::UInt8,top.convertToFormat(QImage::Format_RGB888).constBits());
    textureArray_->setData(0,5,QOpenGLTexture::RGB,QOpenGLTexture::UInt8,bottom.convertToFormat(QImage::Format_RGB888).constBits());
    textureArray_->setMinMagFilters(QOpenGLTexture::Nearest,QOpenGLTexture::Nearest);
    textureArray_->setWrapMode(QOpenGLTexture::ClampToEdge);

    const GLfloat vertexs[]{
        //vertex points
        //left side
        -1.0,+1.0,+1.0,
        -1.0,-1.0,+1.0,
        -1.0,-1.0,-1.0,
        -1.0,+1.0,-1.0,
        //behind side
        -1.0,+1.0,-1.0,
        -1.0,-1.0,-1.0,
        +1.0,-1.0,-1.0,
        +1.0,+1.0,-1.0,
        //right side
        +1.0,+1.0,-1.0,
        +1.0,-1.0,-1.0,
        +1.0,-1.0,+1.0,
        +1.0,+1.0,+1.0,
        //front side
        -1.0,+1.0,+1.0,
        +1.0,+1.0,+1.0,
        +1.0,-1.0,+1.0,
        -1.0,-1.0,+1.0,
        //top side
        -1.0,+1.0,-1.0,
        +1.0,+1.0,-1.0,
        +1.0,+1.0,+1.0,
        -1.0,+1.0,+1.0,
        //bottom side
        -1.0,-1.0,-1.0,
        -1.0,-1.0,+1.0,
        +1.0,-1.0,+1.0,
        +1.0,-1.0,-1.0,

        //texture points
        //left side
        +0.0,+0.0,
        +0.0,+1.0,
        +1.0,+1.0,
        +1.0,+0.0,
        //behind side
        +0.0,+0.0,
        +0.0,+1.0,
        +1.0,+1.0,
        +1.0,+0.0,
        //right side
        +0.0,+0.0,
        +0.0,+1.0,
        +1.0,+1.0,
        +1.0,+0.0,
        //front side
        +1.0,+0.0,
        +0.0,+0.0,
        +0.0,+1.0,
        +1.0,+1.0,
        //top side
        +0.0,+1.0,
        +1.0,+1.0,
        +1.0,+0.0,
        +0.0,+0.0,
        //bottom side
        +0.0,+0.0,
        +0.0,+1.0,
        +1.0,+1.0,
        +1.0,+0.0,
    };
    vbo_.create();
    vbo_.bind();
    vbo_.allocate(vertexs,sizeof(vertexs));;
}

void BoxShader::render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix)
{
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_CULL_FACE);

    program_.bind();
    vbo_.bind();
    f->glActiveTexture(GL_TEXTURE0 + 0);
    program_.setUniformValue("pMatrix",pMatrix);
    program_.setUniformValue("vMatrix",vMatrix);
    program_.setUniformValue("mMatrix",mMatrix);
    program_.setUniformValue("sTexture",0);

    program_.enableAttributeArray(0);
    program_.enableAttributeArray(1);
    program_.setAttributeBuffer(0,GL_FLOAT,0,3,3*sizeof(GLfloat));
    program_.setAttributeBuffer(1,GL_FLOAT,6 * 4 * 3 * sizeof(GLfloat),2,2*sizeof(GLfloat));
    textureArray_->bind();
    f->glDrawArrays(GL_QUADS,0,24);
    program_.disableAttributeArray(0);
    program_.disableAttributeArray(1);

    textureArray_->release();
    vbo_.release();
    program_.release();
    f->glDisable(GL_DEPTH_TEST);
    f->glDisable(GL_CULL_FACE);
}
