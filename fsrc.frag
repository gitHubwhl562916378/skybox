#version 330
uniform sampler2DArray sTexture;
in float vid;
in vec2 vTextureCoord;
out vec4 fragColor;

void main(void)
{
    vec3 texCood = vec3(vTextureCoord.st,floor(vid/4));
    fragColor = texture(sTexture,texCood);
}
