#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include <QTimer>
#include "boxshader.h"
#include "torusrender.h"

class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

private:
    BoxShader boxShader_;
    TorusRender toruRender_;
    QVector3D camera_;
    QMatrix4x4 pMatrix_;
    QTimer tm_;
    float angleX_ = 0, angleY_ = 0, angleZ_ = 0;

private slots:
    void slotTimeout();
};

#endif // WIDGET_H
