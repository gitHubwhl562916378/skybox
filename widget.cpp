#include <QWheelEvent>
#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    connect(&tm_,SIGNAL(timeout()),this,SLOT(slotTimeout()));
    tm_.start(40);
}

Widget::~Widget()
{

}

void Widget::initializeGL()
{
    camera_.setX(4.9);
    camera_.setY(4.9);
    camera_.setZ(4.9);
    toruRender_.initsize(0.9,0.5,13,30);
    boxShader_.initsize(256,256,QImage("side1.png"),QImage("side2.png"),QImage("side3.png"),QImage("side4.png"),QImage("side5.png"),QImage("side6.png"));
}

void Widget::resizeGL(int w, int h)
{
    pMatrix_.setToIdentity();
    pMatrix_.perspective(80,float(w)/h,0.01f,100.0f);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0, 0.0, 0.0, 0.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(camera_,QVector3D(-4.9,-4.9,-4.9),QVector3D(-1.0,1.0,-1.0));

    QMatrix4x4 mMatrix;
    mMatrix.rotate(angleX_,1,0,0);
    mMatrix.rotate(angleY_,0,1,0);
    mMatrix.rotate(angleZ_,0,0,1);
    mMatrix.scale(20);
    boxShader_.render(f,pMatrix_,vMatrix,mMatrix);

    mMatrix.setToIdentity();
    mMatrix.translate(-1.0,-4.9,-0.5);
    mMatrix.scale(2);
    mMatrix.rotate(-angleY_,0,1,0);
    toruRender_.render(f,pMatrix_,vMatrix,mMatrix);
}

void Widget::slotTimeout()
{
    angleY_ += 5;
    update();
}
