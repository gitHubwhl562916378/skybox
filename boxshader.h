#ifndef BOXSHADER_H
#define BOXSHADER_H

#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QImage>
class BoxShader
{
public:
    BoxShader() = default;
    ~BoxShader();
    void initsize(int width,int height,QImage &left,QImage &bebind,QImage &right,QImage &front,QImage &top,QImage &bottom);
    void render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix);

private:
    QOpenGLTexture *textureArray_{nullptr};
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
};

#endif // BOXSHADER_H
