#version 330
uniform mat4 pMatrix,vMatrix,mMatrix;
layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec2 aTextureCood;
smooth out vec2 vTextureCoord;
out float vid;

void main(void)
{
    gl_Position = pMatrix * vMatrix * mMatrix * vec4(aPosition,1);
    vTextureCoord = aTextureCood;
    vid = gl_VertexID;
}
